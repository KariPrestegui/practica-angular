import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MostrarComponent } from '../components/mostrar/mostrar.component';
import { FormularioComponent } from '../components/formulario/formulario.component';
import { ActualizarComponent } from '../components/actualizar/actualizar.component';

//Le digo la ruta y dónde encontrar el componente
const routes: Routes = [
  {path: 'mostrar', component: MostrarComponent },
  {path: 'ingresar', component: FormularioComponent },
  {path: 'actualizar/:id', component: ActualizarComponent },
  {path: '**', component: FormularioComponent}
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    //Tiene que ser forRoot, porque si no estamos hablando de hijos
    RouterModule.forRoot(routes)
  ],
  //Se exporta la clase con las rutas
  exports: [RouterModule]
})
export class AppRoutingModule { }
