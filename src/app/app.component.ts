import { Component } from '@angular/core';

//Decorador: funcionalidad que se le aplica a una clase y 
//modifica el comportamiento final
@Component({
  selector: 'app-root', //Etiqueta que va a mostrar el selector
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Hoooooooooolaaaaaaa';
}
