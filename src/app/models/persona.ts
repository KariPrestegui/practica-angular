export interface Persona{
    id: String;
    nombre: String;
    appaterno: String;
    apmaterno: String;
    edad: String;
    correo: String;
    password: String;
    profesion: String
}