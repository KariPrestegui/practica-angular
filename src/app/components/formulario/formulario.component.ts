import { Component, OnInit } from '@angular/core';
import { PersonaService } from '../../service/persona.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  constructor(
    //Instancia de clase personaService
    public personaService : PersonaService
    ) { }

  ngOnInit(): void {
  }

  //Trae los datos del formulario
  agregarDatos(newId: HTMLInputElement, newNombre: HTMLInputElement, newAppaterno: HTMLInputElement, newApmaterno: HTMLInputElement, newEdad: HTMLInputElement,
     newCorreo: HTMLInputElement, newPassword: HTMLInputElement, newProfesion: HTMLInputElement){

      //Manda a llamar del servicio el método para agregar y le envía los valores de la persona
      this.personaService.agregarPersona({
        id: newId.value,
        nombre: newNombre.value,
        appaterno: newAppaterno.value,
        apmaterno: newApmaterno.value,
        edad: newEdad.value,
        correo: newCorreo.value,
        password: newPassword.value,
        profesion: newProfesion.value
      }); 

      alert("Persona: " +newNombre.value+ " " +newAppaterno.value+ " agregada con éxito");

      //Limpia los campos
    newId.value='';
    newNombre.value=''; 
    newAppaterno.value='';
    newApmaterno.value='';
    newEdad.value='';
    newCorreo.value='';
    newPassword.value='';
    newProfesion.value='';
    newNombre.focus();

    //Cancelar comportamiento por defecto de la función
    //return false;
  }
}
