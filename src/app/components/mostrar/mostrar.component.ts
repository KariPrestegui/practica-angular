import { Component, OnInit} from '@angular/core';
import { PersonaService } from '../../service/persona.service';
import { Persona } from 'src/app/models/persona';


@Component({
  selector: 'app-mostrar',
  templateUrl: './mostrar.component.html',
  styleUrls: ['./mostrar.component.css']
})
export class MostrarComponent implements OnInit {

  personas: Persona[];

  constructor(
    //Se instancia el objeto de personaService
    public personaService: PersonaService
  ) { }

  ngOnInit(): void {
    //Al inicializar el componente se manda a llamar a la función para mostrar a la persona 
    this.personas = this.personaService.mostrarPersona();
  }

}
