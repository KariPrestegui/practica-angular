import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Persona } from '../../models/persona';
import { PersonaService } from '../../service/persona.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-actualizar',
  templateUrl: './actualizar.component.html',
  styleUrls: ['./actualizar.component.css']
})
export class ActualizarComponent implements OnInit {

  persona: Persona;
  public personaForm: FormGroup;
  
  constructor(private route: ActivatedRoute,
    //Se instancia de personaService
    public personaService: PersonaService,
    private fb: FormBuilder,) {
   }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.leerDatos(id);
  }

  actualizarFormInitilize(persona: Persona){
      this.personaForm = this.fb.group({
        id: [persona.id],
        nombre: [persona.nombre]
      });
  }

  leerDatos(id: string){
    this.persona = this.personaService.mostrarPerson(id);
    this.actualizarFormInitilize(this.persona);




  }

}
