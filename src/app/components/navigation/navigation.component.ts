import { Component, OnInit } from '@angular/core';
//Se importa tipo router
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  constructor(
    //Se instancia el router
    private  router: Router
  ) { }

  ngOnInit(): void {
  }

  // //Función para hacer el ruteo
  // //navegarHaciaMostrar(){
  //   //Utilizar la instancia inyectada, esto es para la ruta en el navegador
  //   // this.router.navigate(['/mostrar'])
  // }

  // //Función para hacer el ruteo
  // navegarHaciaIngresar(){
  //   //Utilizar la instancia inyectada, esto es para la ruta en el navegador
  //   this.router.navigate(['/ingresar'])
  // }

}
