import { Component, OnInit, Input } from '@angular/core';
import { Persona } from 'src/app/models/persona';
import { PersonaService } from '../../service/persona.service';

@Component({
  selector: 'app-vista-persona',
  templateUrl: './vista-persona.component.html',
  styleUrls: ['./vista-persona.component.css']
})
export class VistaPersonaComponent implements OnInit {
  
  @Input() pasarDatos: Persona;
  
  constructor(
    public personaService: PersonaService
  ) { }

  ngOnInit(): void {
  }

  borrarPersona(pasarDatos: Persona){
    this.personaService.borrarPersona(pasarDatos);
  }

  actualizarPersona(pasarDatos: Persona){
    this.personaService.actualizarPersona(pasarDatos);
  }

}
