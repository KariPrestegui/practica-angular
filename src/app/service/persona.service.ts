import { Injectable } from '@angular/core';
import { Persona } from '../models/persona';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  personas: Persona[];

  constructor() {
    //Se inicializa en el constructor el array de tipo Persona
    this.personas =[];
   }

   //se obtiene a la persona del localStorage
   mostrarPersona(){
     if(localStorage.getItem('personas')===null){
      return this.personas;
     }else{
      return this.personas = JSON.parse(localStorage.getItem('personas'));
    }
   }

     //Obtener persona por id
     mostrarPerson(id: string){
       this.personas = JSON.parse(localStorage.getItem('personas'));
       console.log(this.personas.find(element => element.id=== id));
      return this.personas.find(element => element.id=== id);
     }
     
   
    

   //Se agrega a la persona, tanto en el arreglo local para que se muestre en la vista
   //Como en el localStorage para que se guarde
   agregarPersona(persona: Persona){
     if(localStorage.getItem('personas')===null){
      this.personas.push(persona);
      localStorage.setItem('personas', JSON.stringify(this.personas));
     }else{
       this.personas = JSON.parse(localStorage.getItem('personas'));
       this.personas.push(persona);
       localStorage.setItem('personas', JSON.stringify(this.personas));
     }
   }

   //Se borra a la persona del arreglo local y del localStorage
   borrarPersona(persona: Persona){
      for(let i=0; i<this.personas.length; i++){
        if(persona == this.personas[i]){
          this.personas.splice(i, 1);
          localStorage.setItem('personas', JSON.stringify(this.personas));
        }
      }
   }

   //Se actualiza a la persona en el arreglo local
   actualizarPersona(persona: Persona){

   }
  
}
